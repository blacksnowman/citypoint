<?PHP
class ApiController implements IController 
{
	public $params,$request;
	private $model,$logger,$helper;
	private $uploadDir = ROOT_DIR."/data/";
	private $maxFileSize = 1024*1024*64;
	private $recsOnPage = 10;
	private $errors = array();
	private $currentPage = 1;
	private $daysIntervalLimit = 7;
	
	public $dataTypes = array("json","xml");
	public $dataType = "json";
	
	public function indexAction(){
		$info = ['version'=>'1.0', 'name'=>'Web API', 'data_type'=>$this->dataType ];
		$tmp = array_merge($info,$_SERVER);
		$this->helper->retJSON($tmp);
	}
	
	public function phpinfoAction(){
		$tmp = phpinfo();
		#return $this->helper->retJSON($tmp);
	}

	public function testXmlAction() {
	    $data['data'] = $_SERVER;
	    $data['result'] = true;
        if($this->dataType=='xml') {
            $this->helper->retXML($data);
        } else {
            $this->helper->retJSON($data);
        }
    }

    private function checkInputData(){
        if(isset($_POST['recsOnPage'])AND($_POST['recsOnPage']>0)){
            $this->recsOnPage = $_POST['recsOnPage'];
        } else {
            $this->errors[] = "Undefined record on page counter.";
            # return false;
        }

        if(isset($_POST['request'])) {
            $this->request['request'] = empty($_POST['request']) ? 0 : $_POST['request'];
        }

        if(isset($_POST['page'])AND($_POST['page']>0)) {
            $this->currentPage = $_POST['page'];
        } else {
            $this->errors[] = "Undefined page number.";
            # return false;
        }

        if(isset($_POST['dataType'])){
            if(in_array($_POST['dataType'],$this->dataTypes)){
                $this->dataType = $_POST['dataType'];
            }
        }

        if(isset($_POST['startDate']) AND isset($_POST['endDate'])){
            if(preg_match('#\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}#', $_POST['startDate'], $out)){
                $start = str_replace("T"," ",$_POST['startDate']);
                if(preg_match('#\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}#', $_POST['endDate'], $out)){
                    $end = str_replace("T"," ",$_POST['endDate']);
                    If($this->helper->compareDates($start, $end, $this->daysIntervalLimit)){
                        $this->request['startDate'] = $start;
                        $this->request['endDate'] = $end;
                        return true;
                    } else {
                        $this->errors[] = "Date interval is more than $this->daysIntervalLimit days";
                    }
                } else {
                    $this->errors[] = "Wrong end date";
                }
            } else {
                $this->errors[] = "Wrong start date";
            }
        } else {
            $this->errors[] = "Date interval is not set";
        }
        return false;
    }

	public function getDataAction(){
	    $tmp = array();
	    $tmp['result'] = false;
        # $tmp['debug'] = $_POST;
        if(!$this->checkInputData()){
            $tmp['result'] = false;
        } else {
            $count = $this->model->getDataCount($this->request['startDate'], $this->request['endDate']);
            $pages = floor($count / $this->recsOnPage);
            if($this->currentPage>$pages) {
                $this->errors[] = "Request page number is greater than total pages count.";
                $tmp['result'] = false;
            }

            $offset = $this->currentPage * $this->recsOnPage;
            $limit = $this->recsOnPage;

            $data = $this->model->getData($this->request['startDate'], $this->request['endDate'], $offset, $limit);
            $tmp['result'] = true;
            $tmp['count'] = $count;
            $tmp['pages'] = $pages;
            $tmp['currentPage'] = $this->currentPage;
            $tmp['dataType'] = $this->dataType;
            $tmp['data'] = $data;

            if($this->request['request']==1) {
                $this->logger->logApiRequest($this->request['startDate'], $this->request['endDate'], $count, $this->dataType);
            }
        }

        if(count($this->errors)>0) {
            $tmp['errors'] = $this->errors;
        }

        if($this->dataType=='xml') {
            $this->helper->retXML($tmp);
        } else {
            $this->helper->retJSON($tmp);
        }

    }
	
	public function getUploadsAction(){
		$tmp = $this->logger->getFileLog();
		$this->helper->retJSON($tmp);
	}

	/*
	public function dbLoadAction(){
		$filename = ROOT_DIR."/data/test.csv";
		$start = time();
		$this->model->parseDataFile($filename);
		$tmp['diff'] = time() - $start;
		$tmp['result'] = true;
		$this->helper->retJSON($tmp);
	}
	*/

	public function uploadAction(){
		$target_dir = $this->uploadDir;
		$tmp['result'] = true;

        $this->model->clearTracker();
		foreach($_FILES as $i=>$file) {
			$tmp['files'][$i] = $file;
			$target_file = $target_dir . basename($file["name"]);
			$type = pathinfo($target_file,PATHINFO_EXTENSION);
			
			//$tmp['files'][$i]['name'] = $file['name'];
			$tmp['files'][$i]['uploaded'] = true;
			$tmp['files'][$i]['path'] = $target_file;
			$tmp['files'][$i]['ip'] = $_SERVER['REMOTE_ADDR'];
			// Check if file already exists
			/*
			if (file_exists($target_file)) {
				$tmp['files'][$i]['error']  = "Sorry, file already exists.";
				$tmp['files'][$i]['uploaded'] = false;
				continue;
			}
			*/
			// Check file size
			if ($file["size"] > $this->maxFileSize) {
				$tmp['files'][$i]['error']  = "Sorry, your file is too large.";
				$tmp['files'][$i]['uploaded'] = false;
				continue;
			}
			// Allow certain file formats
			if($type != "csv" ) {
				$tmp['files'][$i]['error']  = "Sorry, only CSV files are allowed.";
				$tmp['files'][$i]['uploaded'] = false;
				continue;
			}

			if ($tmp['files'][$i]['uploaded'] === false) {
				$tmp['files'][$i]['error'] = "Sorry, your file was not uploaded because of errors.";
			} else {
				if (move_uploaded_file($file["tmp_name"], $target_file)) {
					$tmp['files'][$i]['logged'] = $this->logger->logFile($tmp['files'][$i]);
					$tmp['files'][$i]['uploaded'] = true;
					$start = microtime(true);
					$this->model->parseDataFile($target_file);
					$diff = microtime(true) - $start;
					$tmp['files'][$i]['parse_time'] = $diff;
				} else {
					$tmp['files'][$i]['error'] = "Sorry, there was an error uploading your file.";
				}
			}
		}
		$this->helper->retJSON($tmp);
	}
	
	public function __construct() {
		$this->logger = Logger::getInstance();
		$this->model = Tracker::getInstance();
		$this->fc = FrontController::getInstance();
		$this->helper = Helper::getInstance();
		$this->params = $this->fc->getParams();

	}
}