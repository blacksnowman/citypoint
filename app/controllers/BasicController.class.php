<?PHP
abstract class BasicController 
{
	protected $helper, $DB,$params,$fc;
    protected $menu = array(
        "index"=>array("name"=>"Задача"),
        "gps"=>array("name"=>"Данные"),
    );

	public function __construct() {
		$this->DB = DB::getInstance();
		$this->fc = FrontController::getInstance();
		$this->helper = Helper::getInstance();
		$this->params = $this->fc->getParams();
	    $this->setActive();
	}

	public function setActive(){
	    $page = $this->fc->getPage();
	    $this->menu[$page]['active'] = "active";
    }
	
	protected function returnData($retval,$data,$error = null){
		$tmp['result'] = $retval;
		if($retval==true) {
			$tmp['data'] = $data;
		} else {
			$tmp['error'] = $error;
		}
		return $this->helper->retJSON($tmp);
	}
}