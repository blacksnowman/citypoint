<?php

class GpsController extends BasicController implements IController
{
    public function __construct() {
        parent::__construct();

    }

    public function indexAction(){
        $this->view = new IndexView("data.tpl");
        $page = $this->fc->_page;
        $this->view->setTitle($page);
        $this->view->createMenu($this->menu);
        $this->view->render();
    }
}