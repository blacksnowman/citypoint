<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 09.06.2017
 * Time: 18:24
 */
class HtmlParser
{
    private $text,$document;

    public function __construct(){
        $this->document = new Document();
    }

    public function parseText($text){
        $this->text = $text;
        $this->clearText();
        $result = $this->findHtml();
        # $result = $this->text;
        return $result;
    }

    public function clearText(){
        $pattern = '/[\r\n]+/i';
        $replacement = '';
        $this->text = preg_replace($pattern,$replacement,$this->text);
    }

    public function calcTagsNumber(){
        return $this->document->calcTags();
    }

    public function getDom(){
        //return $this->document->elements;
    }

    public function findHtml(){
        $token = new Tokenizer($this->text,$this->document);
        $token->iterate();
        $result = (string)$this->document;
        return $result;
    }
}