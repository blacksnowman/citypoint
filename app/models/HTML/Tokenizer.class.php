<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 10.06.2017
 * Time: 13:08
 */
class Tokenizer
{
    public $text,$document;
    private $buffer;

    public $states = array(
        0=>"readingData",
        1=>"firstTagOpened",
        2=>"firstTagClosed",
        3=>"secondTagOpened",
        4=>"secondTagClosed"
    );

    public $level = 0;
    public $state = 0;

    public function __construct($text,$document){
        $this->text = $text;
        $this->document = $document;
    }

    public function iterate(){
        $arr = str_split($this->text);
        foreach($arr as $i=>$c){
            if(($c=='<')AND($arr[$i+1]!='/')) {
                if($this->state==0) {
                    $this->level++;
                }

                if($this->state==4) {

                }

                $this->state = 1;
                $this->emptyBuffer();
            }elseif(($c=='<')AND($arr[$i+1]=='/')) {
                if($this->state==0) {
                    $this->state = 3;
                }

                if($this->state==4) {
                    $this->state = 3;
                    $this->level--;
                }

                $this->emptyBuffer();
            }elseif($c=='>') {
                # if the next tag is secondary too then we go too previous level

                # close first tag and return to reading data state
                if($this->state==1) {
                    $this->state=0;
                    # echo "<BR>".$this->buffer;
                    $tag = new HtmlTag();
                    $tag->parseBuffer($this->buffer);
                    $this->document->addElement($tag,$this->level);
                    $this->emptyBuffer();
                }

                if($this->state==3){
                    $this->state = 4;
                    $tag = $this->document->findElement($this->level);
                    $tag->setClosed();

                    $this->emptyBuffer();
                }
            } else {
                $this->buffer .= $c;
            }
        }
    }

    public  function getDocument(){
        return $this->document;
    }

    public function emptyBuffer(){
        $this->buffer = "";
    }
}