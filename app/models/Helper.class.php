<?PHP
class Helper
{
	static $_instance;
	
	public static function getInstance() {
		if(!(self::$_instance instanceof self)) 
			self::$_instance = new self();
		return self::$_instance;
	}

	public function compareDates($st,$end,$days){
	    $interval = $days*24*3600;
        $start = new DateTime($st);
        $end = new DateTime($end);
        $diff = $end->getTimestamp() - $start->getTimestamp();
        if($diff>$interval){
            return false;
        } else {
            return true;
        }
    }

    public function getDateInterval($st,$end){
        $start = new DateTime($st);
        $end = new DateTime($end);
        $diff = $end->getTimestamp() - $start->getTimestamp();
        return $diff;
    }
	
	public function xscanDir($dir,$ext = null) {
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
		   if (($entry!=".") AND ($entry!="..")) {
				if($ext!=null){
					if(strpos($entry, $ext) OR strpos($entry, strtoupper($ext))) {
						$tmp[] = $entry;
					}
				} else {
					$tmp[] = $entry;
				}
			}
		}
		$d->close();
		return $tmp;
	}
	
	public function xreadDir($dir) {
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
		   if (($entry!=".") AND ($entry!="..")) {
				if(is_dir(sprintf("%s/%s",$dir,$entry))){
					$tmp[] = $entry;
				}
			}
		}
		$d->close();
		return $tmp;
	}
	
	public function retJSON($json) {
		header('Content-Type: application/json');
		echo json_encode($json);
	}

	public function retXML($data){
        $xml = new SimpleXMLElement('<xml/>');
        foreach($data as $key=>$value) {
            $this->xmlAdd($xml,$key,$value);
        }

        Header('Content-type: text/xml');
        print($xml->asXML());
	}

    public function xmlAdd(&$node, $key, $value){
	    if(is_array($value)) {
	        if(is_numeric($key)){
	            $name = "row";
            } else {
	            $name = $key;
            }
	        $newNode = $node->addChild($name);
	        # $newNode->addAttribute("type","array");
	        foreach($value as $k=>$v) {
                $this->xmlAdd($newNode, $k, $v);
            }
        } else {
            $node->addChild($key,$value);
        }
    }

	public function retTEXT(){
        header("Content-Type: plain/text");
    }

    public function retTextArea($text,$rows){
	    $str = "<textarea cols=80 rows=$rows>$text</textarea>";
	    return $str;
    }
}
?>