<?PHP
class Logger
{
    private $DB,$helper;
    public $logTable = "files";
    public $apiLogTable = "requestLogApi";

    static private $_instance;

    public function __construct(){
        $this->DB = DB::getInstance();
        $this->helper = Helper::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$_instance instanceOf self)
            self::$_instance = new self;
        return self::$_instance;
    }

    public function getFileLog()
    {
        $sql = sprintf("SELECT * FROM %s ORDER BY `date`", $this->logTable);
        $data = $this->DB->getResults($sql);
        return $data;
    }

    public function clearLogByIP($ip)
    {
        $sql = sprintf("DELETE FROM %s WHERE ip=:ip", $this->logTable);
        try {
            $sth = $this->DB->prepare($sql);
            $sth->bindParam(":ip", $ip);
            $sth->execute();
            return true;
        } catch (PDOException $e) {
            # echo $e->getMessage();
            $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * @param $data
     * @return bool
     */
    public function logApiRequest($start,$end,$count,$datatype) {
        $interval = $this->helper->getDateInterval($start,$end);
        $sql = sprintf("INSERT INTO %s (ip,`interval`,datatype, points, `date`) VALUES(INET_ATON(:ip), :interval, :datatype, :points, NOW())", $this->apiLogTable);
        try {
            $sth = $this->DB->prepare($sql);
            $sth->bindParam(":ip", $_SERVER['REMOTE_ADDR']);
            $sth->bindParam(":datatype", $datatype);
            $sth->bindParam(":points", $count);
            $sth->bindParam(":interval",$interval);
            # $sth->bindParam(":page",$currentPage);
            # $sth->bindParam(":pages",$pages);
            $sth->execute();
            return true;
        } catch (PDOException $e) {
            # echo $e->getMessage();
            $this->error = $e->getMessage();
            return false;
        }
    }

    public function logFile($file)
    {
        $sql = sprintf("INSERT INTO %s (`filename`,`size`,`path`,`ip`,`date`) VALUES(:filename, :size, :path, INET_ATON(:ip), NOW()) ON DUPLICATE KEY update `date`=NOW(), size=:size, ip=INET_ATON(:ip), path=:path", $this->logTable);
        try {
            $sth = $this->DB->prepare($sql);
            $sth->bindParam(":filename", $file['name']);
            $sth->bindParam(":size", $file['size']);
            $sth->bindParam(":path", $file['path']);
            $sth->bindParam(":ip", $file['ip']);
            $sth->execute();
            return true;
        } catch (PDOException $e) {
            # echo $e->getMessage();
            $this->error = $e->getMessage();
            return false;
        }
    }
}