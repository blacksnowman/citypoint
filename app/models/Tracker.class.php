<?PHP
class Tracker
{
	private $DB;
	public $delimiter = ";";
	public $sqlTable = "tracker";
	static private $_instance;
	
	
	public static function getInstance() {
		if(!self::$_instance instanceOf self)
			self::$_instance = new self;
		return self::$_instance;
	}
	
	public function __construct(){
		$this->DB = DB::getInstance();
	}

	public function getDataCount($start,$end){
        $sql = sprintf("SELECT count(*) as cnt FROM %s WHERE `date` BETWEEN :startdate AND :enddate ORDER BY `date` DESC",$this->sqlTable);
        try {
            $sth = $this->DB->prepare($sql);
            $sth->bindParam(":startdate",$start);
            $sth->bindParam(":enddate",$end);
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);
            return $row['cnt'];
        } catch(PDOException $e) {
            # echo $e->getMessage();
            $this->error = $e->getMessage();
            return false;
        }
    }

	public function getData($start,$end,$offset,$limit){
        $sql = sprintf("SELECT * FROM %s WHERE `date` BETWEEN :startdate AND :enddate ORDER BY `date` DESC LIMIT %d,%d",$this->sqlTable,$offset, $limit);
        try {
            $sth = $this->DB->prepare($sql);
            $sth->bindParam(":startdate",$start);
            $sth->bindParam(":enddate",$end);
            $sth->execute();
            while($row = $sth->fetch(PDO::FETCH_ASSOC)){
                $res[] = $row;
            }
            return $res;
        } catch(PDOException $e) {
            # echo $e->getMessage();
            $this->error = $e->getMessage();
            return false;
        }
    }

    public function write2file($str){
        $rh = fopen(ROOT_DIR."/data/sql.log", 'a');
        fputs($rh,$str);
        fclose($rh);
    }

    public function checkArray($arr){
        foreach ($arr as $k=>$v){
            if($v==""){
                return false;
            } else {

            }
        }
        return true;
    }

    public function clearTracker(){
        $this->DB->exec_only("TRUNCATE TABLE tracker");
    }

    public function parseDataFile2($filename){
        $sql = sprintf("INSERT INTO %s(`date`,`speed`,`lon`,`lat`) VALUES (:date,:speed,:lon,:lat)", $this->sqlTable);
        $sth = $this->DB->prepare($sql);
        $this->DB->beginTransaction();
        $c = 0;
        $rh = fopen($filename, 'r');
        while (!feof($rh)) {
            $line = trim(fgets($rh));
            $arr = explode($this->delimiter,$line);
            if($this->checkArray($arr)===false){
                $this->write2file("Wrong data detected on line #".$c.": [$line]\n");
            } else {
                $sth->bindParam(":date",$arr[0]);
                $sth->bindParam(":speed",$arr[1]);
                $sth->bindParam(":lon",$arr[2]);
                $sth->bindParam(":lat",$arr[3]);
                $sth->execute();
            }
            $c++;
        }
        fclose($rh);
        $this->DB->commit();
        return true;
    }

	public function parseDataFile($filename){
		if (!file_exists($filename)) {
			return false;
		}
		$bulk_limit = 4096;

        #set_time_limit(60);
		$sql = sprintf("INSERT INTO %s(`date`,`speed`,`lon`,`lat`) VALUES ", $this->sqlTable);
        $part_insert = "";

		$rh = fopen($filename, 'r');
		$i=0;$c=0;
		while (1) {
			$line = trim(fgets($rh));
			$arr = explode($this->delimiter,$line);
			if($this->checkArray($arr)===false){
                $this->write2file("Wrong data detected on line #".$c.": [$line]\n");
            } else {
                $point = new Point($arr[0], $arr[1], $arr[2], $arr[3]);
                if(($i>0) and ($i<$bulk_limit)){
                    $part_insert .= ",";
                }
                $part_insert .= sprintf("('%s',%s,%s,%s)", $point->date, $point->speed, $point->lon, $point->lat);

                if($i==($bulk_limit-1)){
                    $this->DB->exec_only($sql.$part_insert.";");
                    #$this->write2file("[$c][$i]: ".$sql.$part_insert."\n");
                    $i = 0;
                    $part_insert = "";
                } else {
                    $i++;
                }
            }

            if(feof($rh)){
                $this->DB->exec_only($sql.$part_insert.";");
                # $this->write2file("[$c][$i]: ".$sql.$part_insert."\n");
                break;
            }
			$c++;
		}

		fclose($rh);
		return true;
	}
}
?>