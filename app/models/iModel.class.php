<?PHP
interface iModel 
{
	public function getAll($offset = null, $limit = null);
	public function getById($id);
	public function update();
	public function create();
	public function delete($id);
}
?>
