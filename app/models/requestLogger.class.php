<?PHP
class requestLogger
{
	private $DB;
	public function __construct($request)
	{
		$this->DB = DB::getInstance();
		if (count($request)>0)
		{
			foreach($request as $k=>$v)
			{
				$this->$k = $v;
			}
		}
		else
		{
			
		}
	}
	
	public function logRequest() {
		$sql = "INSERT INTO requestLog(requestTime,redirectStatus,requestMethod,remoteAddr,httpUserAgent,requestURI) VALUES(:requestTime,:redirectStatus,:requestMethod,INET_ATON(:remoteAddr),:httpUserAgent,:requestURI)";
			
		try
		{
			$sth = $this->DB->prepare($sql);
			$sth->bindParam(':requestTime',$this->requestTime,PDO::PARAM_INT);
			$sth->bindParam(':redirectStatus',$this->redirectStatus,PDO::PARAM_INT);
			$sth->bindParam(':requestMethod',$this->requestMethod,PDO::PARAM_STR);
			$sth->bindParam(':remoteAddr',$this->remoteAddr,PDO::PARAM_STR);
			$sth->bindParam(':httpUserAgent',$this->httpUserAgent,PDO::PARAM_STR);
			$sth->bindParam(':requestURI',$this->requestURI,PDO::PARAM_STR);
			$sth->execute();
			return true;
		}
		catch (PDOException $e) 
		{ 
			print $e->getMessage();
			return false;
		}
	}
}
?>