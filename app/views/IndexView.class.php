<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 18.06.2017
 * Time: 20:56
 */
class IndexView extends SmartView
{
	public $cssExt = array(
		#"/css/uploadfile.css"
	);
	
	public $jsExt = array(
		#"/js/vendor/jquery.ui.widget.js",
		#"/js/jquery.fileupload.js"
	);
	
    public function __construct($tpl) {
        parent::__construct();
        $this->template = $tpl;
        $this->smarty->assign("random",rand(0,9999));
        #$this->loadCss($this->cssExt);
        #$this->loadJs($this->jsExt);

    }

    public function show() {
        $this->smarty->assign('name', 'Move task');
        $this->smarty->display($this->template);
    }

}