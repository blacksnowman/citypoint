<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 09.06.2017
 * Time: 14:32
 */
abstract class SmartView
{
    protected $template = "index.tpl";
    protected $smarty;

    public $js = array(
        "/js/jquery-3.2.1.min.js",
        "/js/bootstrap.min.js"
    );

    public $css = array(
        "/css/bootstrap.min.css",
        "/css/bootstrap-theme.min.css",
        "/css/common.css",
    );

    private function defaultJS(){
        if (file_exists(JS_DIR . "/" . $this->local_js)) {
            array_push($this->js, "/js/".$this->local_js);
        }
    }

    private function defaultCSS(){
        if (file_exists(CSS_DIR . "/" . $this->local_css)) {
            array_push($this->css, "/css/".$this->local_css);
        }
    }

    public function loadJs($list = null) {
        if(is_array($list)) {
            foreach ($list as $k => $v) {
                array_push($this->js, $v);
            }
        }
        $this->smarty->assign('js_list',$this->js);
    }

    public function loadCss($list = null) {
        if(is_array($list)AND(count($list)>0)) {
            foreach ($list as $k => $v) {
                array_push($this->css, $v);
            }
        }
        $this->smarty->assign('css_list',$this->css);
    }

    public function createMenu($menu) {
        $this->smarty->assign('menu',$menu);
    }

    public function __construct() {
        # print_r(spl_autoload_functions());
        $this->smarty = new Smarty();
        $this->smarty->error_reporting = E_ALL ^ E_NOTICE;
        $this->smarty->caching = false;
        #$this->smarty->clear_all_cache();
		#$this->smarty->cache_lifetime = 0;
		$this->smarty->compile_check = true;
		$this->smarty->force_compile = true;
        $this->smarty->setPluginsDir(SMARTY_SYSPLUG);
        $this->smarty->setTemplateDir(ROOT_DIR.'/smarty/templates');
        $this->smarty->setCompileDir(ROOT_DIR.'/smarty/templates_c');
        $this->smarty->setCacheDir(ROOT_DIR.'/smarty/cache');
        $this->smarty->setConfigDir(ROOT_DIR.'/smarty/configs');

        $fc = FrontController::getInstance();
        $this->page = $fc->getPage();

        $this->local_css = sprintf("%s.css",$this->page);
        $this->local_js = sprintf("%s.js",$this->page);

        $this->defaultJS();
        $this->defaultCSS();
        $this->loadCss();
        $this->loadJs();
    }

    public function setTitle($title){
        $this->smarty->assign("title",$title);
    }

    public function render() {
        $this->smarty->display($this->template);
		$this->smarty->clearCache($this->template);
    }
}