<?PHP
define("DBCONFIG","config/database.conf");
define("ROOT_DIR", $_SERVER['DOCUMENT_ROOT']);
define("CONFIG_DIR", ROOT_DIR."/config");
define("APP_DIR", ROOT_DIR."/app");
define('SMARTY_DIR', APP_DIR."/libraries/smarty/");
define("SMARTY", SMARTY_DIR."Smarty.class.php");
define("SMARTY_SYSPLUG", SMARTY_DIR."sysplugins");
define("CSS_DIR",ROOT_DIR."/css");
define("JS_DIR",ROOT_DIR."/js");

define('SMARTY_SPL_AUTOLOAD',0);

set_include_path(get_include_path()
    .PATH_SEPARATOR.'app/controllers'
    .PATH_SEPARATOR.'app/models'
    .PATH_SEPARATOR.'app/views'
    .PATH_SEPARATOR.'app/models/HTML'
    .PATH_SEPARATOR.SMARTY_SYSPLUG
);
?>