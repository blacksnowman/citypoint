<?PHP
function my_autoloader($class_name) {
	require_once($class_name.".class.php");
}

function show_arr($arr) {
	foreach ($arr as $key => $val) {
		echo "\n<ul>";
		if (is_array($val)) {
			echo "<il>&nbsp;&nbsp;[<font color=#888888>$key</font>] = >[<font color=#AA5555>$val</font>]</il>\n";
			echo show_arr($val);
		} else {
			echo "<il>&nbsp;&nbsp;[<font color=#888888>$key</font>] => [<font color=#AA5555>$val</font>]</il>\n";
		}
		echo "\n</ul>";
	}
}

function array_swap(&$arr, $num){
    $tmp = $arr[0];
    $arr[0] = $arr[$num];
    $arr[$num] = $tmp;
}

function array_shaker_sort(&$arr){
    $left = 0;
    $right = count($arr)-1;

    do {
        for ($i = $left; $i < $right; $i++) {
            if ($arr[0] < $arr[$i]) {
                array_swap($arr, $i);
            }
        }

        for ($i = $right; $i > $left; $i--) {
            if ($arr[0] > $arr[$i]) {
                array_swap($arr, $i);
            }
        }
        $right -= 1;
    } while ($left<=$right);
}

spl_autoload_register('my_autoloader');
?>