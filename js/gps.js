function getGpsData(page,request){
    var startdate = $("#startDate").val();
    var enddate = $("#endDate").val();
    var recsOnPage = $("#recordsOnPage").val();
    var dateFormat = $("#dateFormat").val();
    var dataType = $("#dataType option:selected").val();

    var sendData = {
        startDate : startdate,
        endDate : enddate,
        recsOnPage : recsOnPage,
        page: page,
        request: request,
        dataType : dataType
    };

    $.post( "/api/getdata", sendData, function(json) {
        if(json.result==true) {
            buildPagination(json.pages,page);
            buildDataTable(json.data);
        } else {
            alert("Ошибка загрузки данных!");
        }
    });
}

function buildDataTable(data){
    $("#gpsDataTable").empty();
    $.each(data, function(key, value) {
        var tr = $("<tr></tr>");
        $.each(value, function(i, cell) {
            var td = $("<td></td>");
            td.text(cell);
            tr.append(td);
        });

        $("#gpsDataTable").append(tr);
    });
}

function buildPagination(pages,currentPage){
    var i=0;
    var maxPages = 10;
    if(pages<maxPages){
        maxPages = pages;
    }

    var nextPage;
    var prevPage;

    $("#paginator").empty();

    if(currentPage>1) {
        prevPage = currentPage-1;
        createPaginationElement("getGpsData(" + prevPage + ",0);", "Prev", 0);
    } else {
        prevPage = 1;
        createPaginationElement("getGpsData(" + prevPage + ",0);", "Prev", 2);
    }


    var firstPaginator = 1;
    var lastPaginator = maxPages;

    if(currentPage>maxPages) {
        lastPaginator = currentPage;
        firstPaginator = currentPage-maxPages;
    }

    for(i=firstPaginator;i<=lastPaginator;i++){
        var active = 0;
        if(i==currentPage) {
            active=1;
        }
        createPaginationElement("getGpsData("+i+",0);",i,active);
    }

    nextPage = currentPage+1;
    if(currentPage<pages){
        createPaginationElement("getGpsData("+nextPage+",0);","Next",0);
    } else {
        createPaginationElement("getGpsData("+nextPage+",0);","Next",2);
    }

}

function createPaginationElement(onclick,text,active){
    var ahref = $("<a></a>");

    ahref.text(text);
    var li = $("<li></li>");
    if(active==1) {
        li.attr("class", "active");
    }
    if(active==2){
        li.attr("class", "disabled");
    } else {
        ahref.attr("onclick", onclick);
    }
    li.append(ahref);
    $("#paginator").append(li);
}