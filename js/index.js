// Variable to store your files
var files;
var uploadApi = "/api/upload";
// Add events
$('input[type=file]').on('change', prepareUpload);

// Grab the files and set them to our variable
function prepareUpload(event){
	files = event.target.files;
}

// process the form
$('#uploadForm').submit(function(event) {
	event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening
	
	 // Create a formdata object and add the files
    var data = new FormData();
    $.each(files, function(key, value) {
        data.append(key, value);
		//console.log(value.name);
    });
	
	// process the form
	$.ajax({
		type        : 'POST', 
		url         :  uploadApi, 
		data        : data, 
		dataType    : 'json', 
		cache: 	false,
		encode       : true,
		processData: false, // Don't process the files
        contentType: false,
	})
	
	 .done(function( data ) {
		 buildStats(data.files);
		 console.log(data);
	 })
});

function buildStats(files){
	var statsBlock = $('#uploadStats');
	statsBlock.empty();
    statsBlock.append("<h4>Загружено файлов: "+files.length+"</h4>");
	var table = $("<table></table>");
	table.attr("class","table table-striped");
    table.append("<thead><tr><th>Имя файла</th><th>Размер (байт)</th><th>Время (сек)</th></tr></thead>");
    var tbody = $("<tbody></tbody>");
    table.append(tbody);
    $.each(files, function(i, file) {
		var tr = $("<tr></tr>");
        $.each(file, function(key, value) {
        	if((key=="name")||(key=="parse_time")||(key=="size")){
                var td = $("<td></td>");
                td.text(value);
                tr.append(td);
            }
        });
        tbody.append(tr);
    });

    statsBlock.append(table);
}
