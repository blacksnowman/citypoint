{extends file='index.tpl'}
{block name=container}
    <div class="container" id="main">
        <div class=row>
            <div class="col-xs-12">
                <div id="alert_list"></div>
            </div>
        </div>
        <div class=row>
            <div class="col-md-4">
                <form name="mainForm">
                    <div class="form-group">
                        <label for="startDate">Начальная дата:</label>
                        <input type="datetime-local" class="form-control" id="startDate" placeholder="Начальная дата" value="2015-11-26T01:00:32">
                    </div>
                    <div class="form-group">
                        <label for="endDate">Конечная дата:</label>
                        <input type="datetime-local" class="form-control" id="endDate" placeholder="Конечная дата" value="2015-11-26T08:00:32">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Формат данных</label>
                        <select class="form-control" id="dataType">
                            <option value="json">JSON</option>
                            <option value="xml">XML</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recordsOnPage">Записей на странице</label>
                        <select class="form-control" id="recordsOnPage">
                            <option value="5">5</option>
                            <option selected value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>

                        </select>
                    </div>
                    <button type="button" class="btn btn-primary" onClick="getGpsData(1,1);">Применить</button>
                </form>
            </div>
            <div class="col-md-8" id="uploadBlock">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Скорость</th>
                        <th>Долгота</th>
                        <th>Широта</th>
                    </tr>
                    </thead>
                    <tbody id="gpsDataTable">

                    </tbody>
                </table>
                <div class="paginationBlock">
                    <ul id="paginator" class="pagination">
                        <!--<li class="page-item"><a class="page-link" href="#">Previous</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
{/block}