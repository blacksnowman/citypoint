<html>
<head>
    <title>{$title}</title>
    <meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
    <meta Http-Equiv="Pragma-directive: no-cache">
    <meta Http-Equiv="Cache-directive: no-cache">
    <meta content="text/html;UTF-8" http-equiv="content-type" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    {foreach $css_list as $css_file}
        <link rel="stylesheet" href="{$css_file}" />
    {/foreach}
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">Menu</a>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav">
                {foreach $menu as $link}
                    {if $link.active eq 'active'}
                        <li class="{$link.active}"><a href="{$link@key}">{$link.name}</a></li>
                    {else}
                        <li><a href="{$link@key}">{$link.name}</a></li>
                    {/if}

                {/foreach}
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://google.ru">Exit</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>
{block name=container}Default{/block}
</body>
{foreach $js_list as $script}
    <script src="{$script}"></script>
{/foreach}
</html>