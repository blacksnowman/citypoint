{extends file='index.tpl'}
{block name=container}
    <div class="container" id="main">
        <div class=row>
            <div class="col-xs-12">
                <div id="alert_list"></div>
            </div>
        </div>
        <div class=row>
            <div class="col-md-6">
                <h4>Исходные данные:</h4>
                <p>Файл в формате csv (разделитель ; ) в котором содержатся точки движения машины (траектория). Каждая строчка файла – точка в истории. Содержит:</p>
                <ul>
                   <li>дату в формате YYYY-mm-dd HH:ii:ss</li>
                    <li>мгновенную скорость в данной точке</li>
                    <li>долготу (lon) в градусах в виде десятичной дроби</li>
                    <li>широту (lat) в градусах в виде десятичной дроби</li>
                </ul>
                <h4>Задача:</h4>
                <p>Создать сервис REST который по запросу будет выдавать данные (историю) за</p>
                <ul><li>определённый промежуток времени</li><li>Ответ может быть в двух форматах: json, xml.</li><li>Каждый зарос должен логироваться в фаил (ip клиента, промежуток времени запроса, формат запрошенных данных, количество точек в результате).</li><li>Если запрошен период более 7 дней сообщать об ошибке.</li><li>Код формирования ответа, проверки входных данных должны иметь unit тесты.</li></ul>
            </div>
            <div class="col-md-6" id="uploadBlock">
                <h4>Решение:</h4>
                <p>Для скорости и удобства работы с данными GPS предлагается загрузить их в СУБД MySQL.
                    Для этого будет ниже организована форма загрузки файлов в БД.
                    Параметры клиентских запросов также будут фиксироваться в БД.</p>
                <h4>Загрузка данных:</h4>
                <!-- <button class="btn btn-primary" onclick="uploadFile();">Загрузить</button>
                <button class="btn btn-primary" onclick="openFileOption();">Выбрать файл</button>-->
				<form id="uploadForm" method="POST" action="/api/upload" >
					<input id="fileupload" type="file" name="files" data-url="/api/upload" multiple>
					<input type="hidden" name="randval" value="{$random}">
					<input type="submit" value="Загрузить" name="submit">
                </form>

                <div id="uploadStats"></div>

                <div class="uploadList">
                    <table class="table">
                    </table>
                </div>
            </div>
        </div>
    </div>
{/block}