<?php
/* Smarty version 3.1.30, created on 2017-06-21 18:12:01
  from "/var/www/html/citypoint/smarty/templates/test.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_594a8cc1c78876_93704734',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1ad8da3808823b59f7f55c1401baee6f969de053' => 
    array (
      0 => '/var/www/html/citypoint/smarty/templates/test.tpl',
      1 => 1498054419,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
  ),
),false)) {
function content_594a8cc1c78876_93704734 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1513698017594a8cc1c709d9_77734989', 'container');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'container'} */
class Block_1513698017594a8cc1c709d9_77734989 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="container" id="main">
        <div class=row>
            <div class="col-xs-12">
                <div id="alert_list"></div>
            </div>
        </div>
        <div class=row>
            <div class="col-md-6">
                <h4>Исходные данные:</h4>
                <p>Файл в формате csv (разделитель ; ) в котором содержатся точки движения машины (траектория). Каждая строчка файла – точка в истории. Содержит:</p>
                <ul>
                   <li>дату в формате YYYY-mm-dd HH:ii:ss</li>
                    <li>мгновенную скорость в данной точке</li>
                    <li>долготу (lon) в градусах в виде десятичной дроби</li>
                    <li>широту (lat) в градусах в виде десятичной дроби</li>
                </ul>
                <h4>Задача:</h4>
                <p>Создать сервис REST который по запросу будет выдавать данные (историю) за</p>
                <ul><li>определённый промежуток времени</li><li>Ответ может быть в двух форматах: json, xml.</li><li>Каждый зарос должен логироваться в фаил (ip клиента, промежуток времени запроса, формат запрошенных данных, количество точек в результате).</li><li>Если запрошен период более 7 дней сообщать об ошибке.</li><li>Код формирования ответа, проверки входных данных должны иметь unit тесты.</li></ul>
            </div>
            <div class="col-md-6" id="uploadBlock">
                <h4>Решение:</h4>
                <p>Для скорости и удобства работы с данными GPS предлагается загрузить их в СУБД MySQL.
                    Для этого будет ниже организована форма загрузки файлов в БД.
                    Параметры клиентских запросов также будут фиксироваться в БД.</p>
                <h4>Загрузка данных:</h4>
                <!-- <button class="btn btn-primary" onclick="uploadFile();">Загрузить</button>
                <button class="btn btn-primary" onclick="openFileOption();">Выбрать файл</button>-->
				<form id="uploadForm" method="POST" action="/api/upload" >
					<input id="fileupload" type="file" name="files" data-url="/api/upload" multiple>
					<input type="hidden" name="randval" value="<?php echo $_smarty_tpl->tpl_vars['random']->value;?>
">
					<input type="submit" value="Загрузить" name="submit">
                </form>

                <div id="uploadStats"></div>

                <div class="uploadList">
                    <table class="table">
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block 'container'} */
}
