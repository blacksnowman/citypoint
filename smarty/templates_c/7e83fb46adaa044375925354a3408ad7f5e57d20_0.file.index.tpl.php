<?php
/* Smarty version 3.1.30, created on 2017-06-21 18:12:01
  from "/var/www/html/citypoint/smarty/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_594a8cc1cc8a80_59445494',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7e83fb46adaa044375925354a3408ad7f5e57d20' => 
    array (
      0 => '/var/www/html/citypoint/smarty/templates/index.tpl',
      1 => 1497876249,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_594a8cc1cc8a80_59445494 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<html>
<head>
    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
    <meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
    <meta Http-Equiv="Pragma-directive: no-cache">
    <meta Http-Equiv="Cache-directive: no-cache">
    <meta content="text/html;UTF-8" http-equiv="content-type" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['css_list']->value, 'css_file');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['css_file']->value) {
?>
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['css_file']->value;?>
" />
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">Menu</a>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menu']->value, 'link');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['link']->key => $_smarty_tpl->tpl_vars['link']->value) {
$__foreach_link_1_saved = $_smarty_tpl->tpl_vars['link'];
?>
                    <?php if ($_smarty_tpl->tpl_vars['link']->value['active'] == 'active') {?>
                        <li class="<?php echo $_smarty_tpl->tpl_vars['link']->value['active'];?>
"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->key;?>
"><?php echo $_smarty_tpl->tpl_vars['link']->value['name'];?>
</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->key;?>
"><?php echo $_smarty_tpl->tpl_vars['link']->value['name'];?>
</a></li>
                    <?php }?>

                <?php
$_smarty_tpl->tpl_vars['link'] = $__foreach_link_1_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://google.ru">Exit</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_557651442594a8cc1cbdca0_55206711', 'container');
?>

</body>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['js_list']->value, 'script');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['script']->value) {
?>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['script']->value;?>
"><?php echo '</script'; ?>
>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</html><?php }
/* {block 'container'} */
class Block_557651442594a8cc1cbdca0_55206711 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Default<?php
}
}
/* {/block 'container'} */
}
