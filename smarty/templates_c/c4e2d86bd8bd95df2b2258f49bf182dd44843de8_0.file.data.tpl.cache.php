<?php
/* Smarty version 3.1.30, created on 2017-06-20 17:23:03
  from "/var/www/html/citypoint/smarty/templates/data.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59492fc76adbf4_81634087',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c4e2d86bd8bd95df2b2258f49bf182dd44843de8' => 
    array (
      0 => '/var/www/html/citypoint/smarty/templates/data.tpl',
      1 => 1497968333,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
  ),
),false)) {
function content_59492fc76adbf4_81634087 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
$_smarty_tpl->compiled->nocache_hash = '174263791459492fc76572c7_38937541';
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_52543171759492fc76a6f38_48591607', 'container');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'container'} */
class Block_52543171759492fc76a6f38_48591607 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="container" id="main">
        <div class=row>
            <div class="col-xs-12">
                <div id="alert_list"></div>
            </div>
        </div>
        <div class=row>
            <div class="col-md-4">
                <form name="mainForm">
                    <div class="form-group">
                        <label for="startDate">Начальная дата</label>
                        <input type="datetime-local" class="form-control" id="startDate" placeholder="Начальная дата" value="2015-11-26T01:00:32">
                    </div>
                    <div class="form-group">
                        <label for="endDate">Конечная дата</label>
                        <input type="datetime-local" class="form-control" id="endDate" placeholder="Конечная дата" value="2015-11-26T08:00:32">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Формат данных</label>
                        <select class="form-control" id="dateFormat">
                            <option value="json">JSON</option>
                            <option value="xml">XML</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="recordsOnPage">Записей на странице</label>
                        <select class="form-control" id="recordsOnPage">
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                        </select>
                    </div>


                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input">
                            Check me out
                        </label>
                    </div>
                    <button type="button" class="btn btn-primary" onClick="getGpsData();">Применить</button>
                </form>
            </div>
            <div class="col-md-8" id="uploadBlock">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Скорость</th>
                        <th>Долгота</th>
                        <th>Широта</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block 'container'} */
}
